cnaTest
=======

Código PHP que contiene una prueba de concepto de la integración PNA.


Cosas que están listas:

* [x] Login
* [ ] Canje de un producto


Notas:

* La URL de "URL_REQUEST_REDIRECT" que LAN.com llamará NO puede ser con un dominio LAN.com (ej: http://www.lan.com/catalogoLanpassAlgo/urlRequestRedirect.html) dado que el request se realiza desde un servidor local (de LAN.com) y para éstos no existe un ProxyPass reverso.
* La URL tiene que ser pública (obvio o no?)