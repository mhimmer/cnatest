<?php
include_once dirname(__FILE__) . '/Logger.php';
include_once dirname(__FILE__) . '/config.php';

$loginXml = '<XML><REQUEST><ACTION>SHOW_LOGIN</ACTION><PARAMS><ID_PARTNER>%d</ID_PARTNER><PARTNER_SESSION_ID>%s</PARTNER_SESSION_ID><URL_ERROR_REDIRECT>%s</URL_ERROR_REDIRECT><URL_REQUEST_REDIRECT>%s</URL_REQUEST_REDIRECT></PARAMS></REQUEST></XML>';
$lanLoginPostdata = sprintf($loginXml, $partnerId, $partnerSessId, $partnerErrorUrl, $partnerInfoUrl);
?>
<html>
  <head>
  </head>
  <body>
    <form action="<?php echo $lanLoginUrl ?>">
      <input type="hidden" name="POSTDATA" value="<?php echo $lanLoginPostdata ?>" />
      <input type="submit" name="submit" value="Login" />
    </form>
  </body>
</html>
