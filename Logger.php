<?php

class Logger
{

  static function log($filename, $str)
  {
    $path = dirname(__FILE__);
    file_put_contents($path.'/'.$filename.'.log', '['.date('Ymd His').'] '.$str."\n", FILE_APPEND);
  }

}
