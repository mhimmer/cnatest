<?php
include_once(dirname(__FILE__) . '/Logger.php');
include_once(dirname(__FILE__).'/config.php');


$rawPost = trim(file_get_contents("php://input"));
if (empty($rawPost))
{
  Logger::log(basename(__FILE__), 'No llegó nada');
  die();
}
// else

$xml = new SimpleXMLElement($rawPost);
$info = $xml->xpath('/XML/REQUEST/PARAMS');
$info = $info[0];

$content = "<XML>
  <REQUEST>
    <ACTION>REQUEST_URL_REDIRECT</ACTION> 
    <PARAMS>
      <MEMBER_NUMBER>".(string)$info->MEMBER_NUMBER."</MEMBER_NUMBER>
      <MEMBER_REWARD_ID>".(string)$info->MEMBER_REWARD_ID."</MEMBER_REWARD_ID>
      <MEMBER_FIRST_NAME>".(string)$info->MEMBER_FIRST_NAME."</MEMBER_FIRST_NAME>
      <MEMBER_LAST_NAME>".(string)$info->MEMBER_LAST_NAME."</MEMBER_LAST_NAME>
      <COUNTRY_ISO>".(string)$info->COUNTRY_ISO."</COUNTRY_ISO>
      <LANGUAGE>".(string)$info->LANGUAGE."</LANGUAGE> 
      <LANPASS_ENROLL_DATE>".(string)$info->LANPASS_ENROLL_DATE."</LANPASS_ENROLL_DATE>
      <PARTNER_SESSION_ID>".(string)$info->PARTNER_SESSION_ID."</PARTNER_SESSION_ID>
      <LAN_TOKEN>".(string)$info->LAN_TOKEN."</LAN_TOKEN>
    </PARAMS>
  </REQUEST>
  <RESPONSE>
		<META>
			<RESPONSEDATETIME>".date('YmdHis')."</RESPONSEDATETIME>
			<REQUESTID>".time()."</REQUESTID>
		</META>
		<RESULT>OK</RESULT>
		<LAN_TOKEN>".(string)$info->LAN_TOKEN."</LAN_TOKEN>
		<PARTNER_SESSION_ID>".(string)$info->PARTNER_SESSION_ID."</PARTNER_SESSION_ID>
		<ID_PARTNER>$partnerId</ID_PARTNER>
		<URL_REDIRECT>$partnerSuccessUrl</URL_REDIRECT>
	</RESPONSE>
</XML>";

Logger::log(basename(__FILE__), $content);
die($content);

