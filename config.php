<?php
$lanLoginUrl = 'https://ssl.lan.com/cgi-bin/canje_kms_partners/paso_show_login.cgi';
$serverBaseUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/cnatest2/';

$partnerId = '25';
$partnerSessId = substr(sha1('PartnerNoAereo25' . date('Y-m-d H:i:s')), 0, 32);
$partnerInfoUrl = $serverBaseUrl . 'loginInfoLan.php';
$partnerErrorUrl = $serverBaseUrl . 'loginError.php';
$partnerSuccessUrl = $serverBaseUrl . 'loginSuccess.php';
